# frozen_string_literal: true

class MergeRequestContextCommit < ApplicationRecord
  include CachedCommit
  include ShaAttribute

  belongs_to :merge_request
  has_many :diff_files, class_name: 'MergeRequestContextCommitDiffFile'

  sha_attribute :sha

  # delete all MergeRequestContextCommit & MergeRequestContextCommitDiffFile for given merge_request & commit SHAs
  def self.delete_bulk(merge_request, commits)
    commit_ids = commits.map(&:sha)
    merge_request.merge_request_context_commits.where(sha: commit_ids).delete_all
  end

  # create MergeRequestContextCommit by given commit sha and it's diff file record
  def self.create_bulk(merge_request, raw_repository, commits)
    context_commit_rows = build_context_commit_rows(merge_request.id, commits)
    context_commit_ids = Gitlab::Database.bulk_insert(self.table_name, context_commit_rows, return_ids: true)
    diff_rows = build_diff_rows(raw_repository, commits, context_commit_ids)
    MergeRequestContextCommitDiffFile.transaction do
      Gitlab::Database.bulk_insert('merge_request_context_commit_diff_files', diff_rows)
    end
  end

  class << self
    private

    def build_context_commit_rows(merge_request_id, commits)
      sha_attribute = Gitlab::Database::ShaAttribute.new
      commits.map.with_index do |commit, index|
        # generate context commit information for given commit
        commit_hash = commit.to_hash.except(:parent_ids)
        sha = sha_attribute.serialize(commit_hash.delete(:id)) # rubocop:disable Cop/ActiveRecordSerialize
        commit_hash.merge(
          merge_request_id: merge_request_id,
          relative_order: index,
          sha: sha,
          authored_date: Gitlab::Database.sanitize_timestamp(commit_hash[:authored_date]),
          committed_date: Gitlab::Database.sanitize_timestamp(commit_hash[:committed_date])
        )
      end
    end

    def build_diff_rows(raw_repository, commits, context_commit_ids)
      sha_attribute = Gitlab::Database::ShaAttribute.new
      diff_rows = []
      diff_order = 0

      commits.each_with_index do |commit, index|
        commit_hash = commit.to_hash.except(:parent_ids)
        sha = sha_attribute.serialize(commit_hash.delete(:id)) # rubocop:disable Cop/ActiveRecordSerialize
        # generate context commit diff information for given commit
        diffs = commit.diffs
        compare = Gitlab::Git::Compare.new(
          raw_repository,
          diffs.diff_refs.start_sha,
          diffs.diff_refs.head_sha
        )
        compare.diffs.each do |diff|
          diff_hash = diff.to_hash.merge(
            sha: sha,
            binary: false,
            merge_request_context_commit_id: context_commit_ids[index],
            relative_order: diff_order
          )
          # Increase order for commit so when present the diffs we can use it to keep order
          diff_order += 1
          diff_rows << diff_hash
        end
      end

      diff_rows
    end
  end
end
