# frozen_string_literal: true

class MergeRequestContextCommitDiffFile < ApplicationRecord
  include Gitlab::EncodingHelper
  include ShaAttribute
  include DiffFile

  belongs_to :merge_request_context_commit, inverse_of: :diff_files

  sha_attribute :sha
  alias_attribute :id, :sha
end
