# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class CreateMergeRequestContextCommitsAndDiffs < ActiveRecord::Migration[5.2]
  DOWNTIME = false

  def change
    create_table :merge_request_context_commits do |t|
      t.datetime_with_timezone :authored_date
      t.datetime_with_timezone :committed_date
      t.integer :relative_order, null: false
      t.binary :sha, null: false
      t.text :author_name
      t.text :author_email
      t.text :committer_name
      t.text :committer_email
      t.text :message
      t.references :merge_request, foreign_key: { on_delete: :cascade }
    end

    create_table :merge_request_context_commit_diff_files, id: false do |t|
      t.binary :sha, null: false
      t.integer :relative_order, null: false
      t.boolean :new_file, null: false
      t.boolean :renamed_file, null: false
      t.boolean :deleted_file, null: false
      t.boolean :too_large, null: false
      t.string :a_mode, null: false, limit: 255
      t.string :b_mode, null: false, limit: 255
      t.text :new_path, null: false
      t.text :old_path, null: false
      t.text :diff
      t.boolean :binary
      t.references :merge_request_context_commit, foreign_key: { on_delete: :cascade }, index: { name: 'index_mr_context_commit_diff_files_on_mr_context_commit_id' }
    end
  end
end
