# Merge request context commits  API

## List MR context commits

Get a list of merge request context commits.

```
GET /projects/:id/merge_requests/:merge_request_iid/context_commits
```

Parameters:

- `id` (required) - The ID or [URL-encoded path of the project](README.md#namespaced-path-encoding) owned by the authenticated user
- `merge_request_iid` (required) - The internal ID of the merge request

```json
[
    {
        "sha": "2d1db523e11e777e49377cfb22d368deec3f0793",
        "relative_order": 0,
        "new_file": false,
        "renamed_file": false,
        "deleted_file": false,
        "too_large": false,
        "a_mode": "100644",
        "b_mode": "100644",
        "new_path": "README.md",
        "old_path": "README.md",
        "diff": "@@ -11,6 +11,6 @@ add them to the `gitlab-test` repo in order to access that blob during developme\n \n 1. Push a new file on a new branch to [gitlab-org/gitlab-test](https://gitlab.com/gitlab-org/gitlab-test).\n 2. Execute `rm -rf tmp/tests` in your gitlab repo.\n-3. Add your branch and its head commit to the `BRANCH_SHA` hash in [`test_env.rb`](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/spec/support/test_env.rb#L7-42).\n+3. Add your branch and its head commit to the `BRANCH_SHA` hash in [`test_env.rb`](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/spec/support/helpers/test_env.rb#L10-68).\n \n In rspec, you can use `create(:project)` to create an instance of `gitlab-test` that has a `path` of `gitlabhq`.\n",
        "binary": false
    }
]
```

## Create MR context commits

Create a list of merge request context commits.

```
POST /projects/:id/merge_requests/:merge_request_iid/context_commits
```

Parameters:

- `id` (required) - The ID or [URL-encoded path of the project](README.md#namespaced-path-encoding) owned by the authenticated user
- `merge_request_iid` (required) - The internal ID of the merge request

```
POST /projects/:id/merge_requests/
```

| Attribute                  | Type    | Required | Description                                                                     |
| ---------                  | ----    | -------- | -----------                                                                     |
| `commits`             | string array | yes | The context commits' sha  |

```json
[
    {
        "id": "6d394385cf567f80a8fd85055db1ab4c5295806f",
        "message": "Added contributing guide\n\nSigned-off-by: Dmitriy Zaporozhets <dmitriy.zaporozhets@gmail.com>\n",
        "parent_ids": [
            "1a0b36b3cdad1d2ee32457c102a8c0b7056fa863"
        ],
        "authored_date": "2014-02-27T10:05:10.000+02:00",
        "author_name": "Dmitriy Zaporozhets",
        "author_email": "dmitriy.zaporozhets@gmail.com",
        "committed_date": "2014-02-27T10:05:10.000+02:00",
        "committer_name": "Dmitriy Zaporozhets",
        "committer_email": "dmitriy.zaporozhets@gmail.com"
    }
]
```

## Delete MR context commits

Delete a list of merge request context commits.

```
DELETE /projects/:id/merge_requests/:merge_request_iid/context_commits
```

Parameters:

- `id` (required) - The ID or [URL-encoded path of the project](README.md#namespaced-path-encoding) owned by the authenticated user
- `merge_request_iid` (required) - The internal ID of the merge request

| Attribute                  | Type    | Required | Description                                                                     |
| ---------                  | ----    | -------- | -----------                                                                     |
| `commits`             | string array | yes | The context commits' sha  |

```json
[
    {
        "id": "6d394385cf567f80a8fd85055db1ab4c5295806f",
        "short_id": "6d394385",
        "created_at": "2014-02-27T10:05:10.000+02:00",
        "parent_ids": [
            "1a0b36b3cdad1d2ee32457c102a8c0b7056fa863"
        ],
        "title": "Added contributing guide",
        "message": "Added contributing guide\n\nSigned-off-by: Dmitriy Zaporozhets <dmitriy.zaporozhets@gmail.com>\n",
        "author_name": "Dmitriy Zaporozhets",
        "author_email": "dmitriy.zaporozhets@gmail.com",
        "authored_date": "2014-02-27T10:05:10.000+02:00",
        "committer_name": "Dmitriy Zaporozhets",
        "committer_email": "dmitriy.zaporozhets@gmail.com",
        "committed_date": "2014-02-27T10:05:10.000+02:00"
    }
]
```
