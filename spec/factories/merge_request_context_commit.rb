# frozen_string_literal: true

FactoryBot.define do
  factory :merge_request_context_commit do
    association :merge_request
    message { '' }
    relative_order { 0 }
    sha { Digest::SHA1.hexdigest(SecureRandom.hex) }
  end
end
